FROM python:3.8.12-slim-buster

ADD . /app
WORKDIR /app
RUN pip install -r requirements.txt
CMD python main.py -c config/config.yaml
