ImageHash==4.2.1
telegram==0.0.1
Pillow==8.4.0
PyYAML==6.0
python-telegram-bot==13.8.1
python-dotenv==0.19.1